<div align="center">
  <h1>Objeto Storage (API de almacenamiento web)</h1>
  <p>Los datos se convirtieron en la nueva materia prima de los negocios. <b>Privacidad y Protección de Datos. #DataPrivacity</b></p>
</div>

<div align="center">
  <a href="https://dataprivacity.com/" target="_blank">
    <img src="assets/images/cabecera/logo.png" width="200">
  </a>
  <h2>@DataPrivacity</h2>
</div>

<div align="center">
  <p>Aprende en nuestras redes:</p>
  <a href="https://DataPrivacity.com/" target="_blank">
    <img src="assets/images/cabecera/globe.svg" width="50">
  </a>
  <a href="https://twitter.com/DataPrivacity" target="_blank">
    <img src="assets/images/cabecera/twitter.svg" width="50">
  </a>
  <a href="https://instagram.com/DataPrivacity" target="_blank">
    <img src="assets/images/cabecera/instagram.svg" width="50">
  </a>
  <a href="https://www.facebook.com/DataPrivacity/" target="_blank">
    <img src="assets/images/cabecera/facebook.svg" width="50">
  </a>
  <a href="https://www.youtube.com/@dataprivacity" target="_blank">
    <img src="assets/images/cabecera/youtube.svg" width="50">
  </a>
  <a href="https://linkedin.com/company/DataPrivacity" target="_blank">
    <img src="assets/images/cabecera/linkedin.svg" width="50">
  </a>
</div>

<div align="center">
  <p>Alojamos proyectos en:</p>
  <a href="https://gitlab.com/DataPrivacity" target="_blank">
    <img src="assets/images/cabecera/gitlab.svg" width="50">
  </a>
  <a href="https://github.com/DataPrivacity" target="_blank">
    <img src="assets/images/cabecera/github.svg" width="50">
  </a>
  <a href="https://drive.google.com/drive/folders/1uHOoUbx83PSKySfbxBOqwlthUA6ofHYP?usp=sharing" target="_blank">
    <img src="assets/images/cabecera/drive.svg" width="50">
  </a>
</div>

<div align="center">
  <p>DR, Cofundador de: DataPrivacity</p>
  <a href="https://twitter.com/soyceros" target="_blank">
    <img src="assets/images/cabecera/cofundador.png" width="50">
  </a>
</div>

Todo el contenido publicado es modificable, si tu quieres colaborar, ves errores o añadir contenido, puedes hacerlo; cumpliendo los siguientes requisitos:

<div align="center">
  <b>“Todos para uno, uno para todos.”</b>
</div>
<br>
<div align="center">
  <b>“Llegar juntos es el principio; mantenerse juntos es el progreso; trabajar juntos es el éxito.”</b>
</div>
<br>
<div align="center">
  <b>Mi trabajo en el software libre está motivado por un objetivo idealista: difundir libertad y cooperación. Quiero motivar la expansión del software libre, reemplazando el software privativo que prohíbe la cooperación, y de este modo hacer nuestra sociedad mejor. Richard Stallman</b>
</div>

# Tabla de contenido
<!-- - [Introduccion](#Introduccion) -->
- [localStorage](#localStorage)
    - [¿Que es localStorage?](#¿Que-es-localStorage?)
    - [Guardar](#Guardar)
    - [Recuperar](#Recuperar)
    - [Eliminar](#Eliminar)
    - [Limpiar todo el Storage](#Limpiar-todo-el-Storage)
- [sessionStorage](#sessionStorage)
    - [¿Que es sessionStorage?](#¿Que-es-sessionStorage?)
    - [Guardar](#Guardar-sessionStorage)
    - [Recuperar](#Recuperar-sessionStorage)
    - [Eliminar](#Eliminar-sessionStorage)
    - [Limpiar todo el Storage](#Limpiar-todo-el-Storage-sessionStorage)

# <a name="localStorage">localStorage</a>
## <a name="¿Que-es-localStorage?">¿Que es localStorage?</a>

La propiedad de sólo lectura localStorage te permite acceder al objeto local Storage; los datos persisten almacenados entre de las diferentes sesiones de navegación. localStorage es similar a sessionStorage. La única diferencia es que, mientras los datos almacenados en localStorage no tienen fecha de expiración, los datos almacenados en sessionStorage son eliminados cuando finaliza la sesion de navegación, lo cual ocurre cuando se cierra la página.

Las características más importantes de este espacio de almacenamiento son:
- Está basado en el formato clave-valor.
- No tiene fecha de expiración, a menos que se limpie el navegador de forma explícita usando los ajustes del propio navegador o por código en JavaScript.
- Puede almacenar hasta 10MB de datos, dependiendo del navegador.
- Sólo los scripts del mismo origen pueden acceder a los datos.
- Se puede compartir entre diferentes pestañas.
- No se envían los datos al servidor, es de uso exclusivo del cliente.
- Los datos que se almacenan son de tipo texto.

## <a name="Guardar">Guardar</a>

```js

// Sintaxis
localStorage.setItem(nombreVariable, valor);
// Ejemplo
localStorage.setItem("Nombre", "Dataprivacity");

```

```js

// Sintaxis
localStorage.nombreVariable = valor;
// Ejemplo
localStorage.Nombre = "Dataprivacity";

```

## <a name="Recuperar">Recuperar</a>

```js

// Sintaxis
let nombreVariable = localStorage.getItem("nombreVariable");
// Ejemplo
let Nombre = localStorage.getItem("Nombre");

```

```js

// Sintaxis
let nombreVariable  = localStorage.nombreVariable;
// Ejemplo
let Nombre  = localStorage.Nombre;

```

## <a name="Eliminar">Eliminar</a>

```js

// Sintaxis
localStorage.removeItem(nombreVariable);
// Ejemplo
localStorage.removeItem(Nombre);

```

## <a name="Limpiar-todo-el-Storage">Limpiar todo el Storage</a>

Eliminar todos los datos del Storage.

```js

// Sintaxis
localStorage.clear();

```

# <a name="sessionStorage">sessionStorage</a>
## <a name="¿Que-es-sessionStorage?">¿Que es sessionStorage?</a>

La propiedad de sólo lectura localStorage te permite acceder al objeto local Storage; los datos persisten almacenados entre de las diferentes sesiones de navegación. localStorage es similar a sessionStorage. La única diferencia es que, mientras los datos almacenados en localStorage no tienen fecha de expiración, los datos almacenados en sessionStorage son eliminados cuando finaliza la sesion de navegación, lo cual ocurre cuando se cierra la página.

Las características más importantes de este espacio de almacenamiento son:
- Está también basado en el formato clave-valor.
- La información persiste hasta que la pestaña o ventana es cerrada, incluso con una recarga de la página.
- Almacena entre 5 y 10MB de datos, dependiendo del navegador que se utilice.
- Sólo los scripts del mismo origen pueden acceder a los datos.
- Está limitado al uso dentro de la misma pestaña, no se puede compartir información entre ventanas o pestañas.
- Es de uso exclusivo del lado cliente.
- El formato de la información que se almacena es de tipo texto.

## <a name="Guardar-sessionStorage">Guardar</a>

```js

// Sintaxis
sessionStorage.setItem("nombreVariable", "valor");
// Ejemplo
sessionStorage.setItem("Nombre", "Dataprivacity");

```

```js

// Sintaxis
sessionStorage.nombreVariable = "valor";
// Ejemplo
sessionStorage.Nombre = "Dataprivacity";

```

## <a name="Recuperar-sessionStorage">Recuperar</a>

```js

// Sintaxis
let nombreVariable = sessionStorage.getItem("nombreVariable");
// Ejemplo
let Nombre = sessionStorage.getItem("Nombre");

```

```js

// Sintaxis
let nombreVariable = sessionStorage.nombreVariable;
// Ejemplo
let Nombre = sessionStorage.Nombre;

```

## <a name="Eliminar-sessionStorage">Eliminar</a>

```js

// Sintaxis
sessionStorage.removeItem(nombreVariable);
// Ejemplo
sessionStorage.removeItem(Nombre);

```

## <a name="Limpiar-todo-el-Storage-sessionStorage">Limpiar todo el Storage</a>

Eliminar todos los datos del Storage.

```js

// Sintaxis
sessionStorage.clear();

```